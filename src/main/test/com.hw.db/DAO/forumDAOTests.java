package com.hw.db.controllers;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.mockito.AdditionalMatchers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

class forumDAOTests {
    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }
}