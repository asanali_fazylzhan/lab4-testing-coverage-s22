package com.hw.db.controllers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.mockito.AdditionalMatchers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

class forumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("Not authorithed user fails to create forum")
    void noLoginWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(null);
            forumController controller = new forumController();
            controller.create(toCreate);
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Владелец форума не найден.")), controller.create(toCreate), "Result when unauthorised user tried to create a forum");
        }
    }

    @Test
    @DisplayName("User fails to create forum because it already exists")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DuplicateKeyException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(toCreate), controller.create(toCreate), "Result for conflict while forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
    
    @Test
    @DisplayName("User fails to create forum because server cannot access DB")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Что-то на сервере.")), controller.create(toCreate), "Result for DB error while server creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
}


